import { writable, get } from "svelte/store";
import { tweened } from "svelte/motion";
import loadMarkup from "../helpers/loadMarkup";

export const videoFileName = writable("");
export const xmlFileName = writable("");
export const video = writable();
export const currentTime = writable();
export const seeking = writable();
export const paused = writable();
export const muted = writable(false);
export const frameRate = writable(24);
export const frame = writable(0);
export const x = tweened(0, { duration: 0 });
export const y = tweened(0, { duration: 0 });

export const videoFile = (() => {
  const { subscribe, update } = writable();
  return {
    subscribe,
    selectFile: (e) => {
      if (!e.target.files[0]) return;
      console.log(e.target.files[0].name);
      const data = URL.createObjectURL(e.target.files[0]);
      videoFileName.set(e.target.files[0].name);
      update((state) => {
        if (state) {
          URL.revokeObjectURL(state);
        }
        return data;
      });
    },
  };
})();

export const markup = (() => {
  const { subscribe, set } = writable();
  return {
    subscribe,
    selectFile: (e) => {
      if (!e.target.files[0]) return;
      console.log(e.target.files[0]);
      xmlFileName.set(e.target.files[0].name);
      const reader = new FileReader();
      reader.readAsText(e.target.files[0]);
      reader.onloadend = function () {
        const data = loadMarkup(reader.result);
        set(data.markup);
        frameRate.set(data.frameRate > 1 ? data.frameRate : 24);
      };
    },
  };
})();

export const size = (() => {
  const variants = {
    setIphone11Vk: { name: "Iphone 11 VK", width: 591, height: 1280 },
    setIphone11Browser: { name: "Iphone 11 Web", width: 650, height: 1130 },
    setXiaomiMax3Vk: { name: "Xiaomi Max 3 VK", width: 640, height: 1240 },
    setXiaomiMax3Browser: {
      name: "Xiaomi Max 3 Web",
      width: 650,
      height: 1130,
    },
    setGalaxyS8Vk: { name: "Galaxy S8 VK", width: 615, height: 1128 },
    setGalaxyS8Web: { name: "Galaxy S8 Web", width: 621, height: 1046 },
  };
  const initialValue = localStorage.getItem("screenSize")
  const { subscribe, set } = writable(
    initialValue ? JSON.parse(initialValue) :  variants.setIphone11Vk
  );
  return {
    subscribe,
    variants,
    setSize: (size) => {
      localStorage.setItem("screenSize", JSON.stringify(size));
      set(size);
    },
  };
})();

export let showFrame = (() => {
    
  const { subscribe, update } = writable(
    localStorage.getItem("showFrame") === "true"
  );
  return {
    subscribe,
    toggle: () =>
      update((state) => {
        localStorage.setItem("showFrame", !state);
        return !state;
      }),
  };
})();
